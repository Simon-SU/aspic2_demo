function init_product() {
    sel_id_prod= django.jQuery("select#id_product") 
    //console.log( 'sel = '+sel_id_prod+' sel val = '+sel_id_prod.val())
    id_product= sel_id_prod.val()
    //console.log( 'id_product '+id_product)
    if (typeof id_product !== 'undefined') {
        sel_id_prod.change(function() {infos_prod()})
        if (id_product === null) django.jQuery("span#info_product").html('search in product CAS numbers, names and formulas<br>choose a product and this displays its CAS number,<br>full name, formula, hazard informations, and update allowed places');
        else infos_prod()
    } else {
        id_product= instance_product_id
        infos_prod()
    }
}
function infos_prod() {
    if ( (id_product === null) || (typeof sel_id_prod.val() !== 'undefined') ) id_product= sel_id_prod.val() 
    if (id_product === null) return 
    //console.log( 'id_product '+id_product)
    django.jQuery("#id_place > option").each(function() { this.disabled= false }); // old bug : sel_id_prod.disabled= false
    django.jQuery.getJSON("/admin/aspic2/get_product_links_hstatement/",{id: id_product}, function(j){ 
        //console.log(' ttt '+j[0].formula+' ttt '+j[0].SDS_link+' - ')
        django.jQuery("span#info_product").html(j[0].SDS_link+'<br>'+j[0].product_link+'<br>'+j[0].formula+'<br>'+j[0].get_hazard_statements_pictos)
        infos_prod_debug= ' - infos_prod - '
        infos_prod_debug+= ' - '+j[0].get_hazard_statements_pictos +' - '
        django.jQuery("#id_place > option").each(function() { 
            //if (j[0].get_hazard_statements_pictos.indexOf('CMR')!=-1) {
            if (j[0].get_hazard_statements_pictos.indexOf('CMR')!=-1 || j[0].get_hazard_statements_pictos.indexOf('T+')!=-1) {
                //infos_prod_debug+='CMR || T+ : '+this.text
	        if (this.text.indexOf(' / 1')==-1 && this.text.indexOf(' / 2')==-1) {
                    infos_prod_debug+=' n12 '//CMR and contains neither / 1 nor / 2'
		    this.disabled= true 
		} else {
                    infos_prod_debug+=' 1o2 '//'CMR and contains / 1 or / 2'
		}
	    } else { 
	        if (this.text.indexOf(' / 1')!=-1) { 
                    infos_prod_debug+= 'n1'//'no CMR || T+ and contains / 1, text : '+this.text
		    this.disabled= true 
		} 
	    }
	});
	if (typeof first_infos_prod !== 'undefined') {
	    django.jQuery("#id_place").val(django.jQuery("#id_place option:first").val())
            //console.log('not first_infos_prod!')
	} else {
	    first_infos_prod= 1
            //console.log('first_infos_prod!')
	}
        //console.log(infos_prod_debug)
    }); 
}


