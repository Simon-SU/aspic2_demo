from django.db import models
from django.utils.text import Truncator
from django.urls import reverse
from django.utils.safestring import mark_safe    
from django.conf import settings
import os.path
import logging, datetime
from django.utils import timezone
from django import forms
from django.templatetags.static import static
from django.db.models import Value
from django.db.models.functions import Concat
from django.db.models import Q

#https://pubchem.ncbi.nlm.nih.gov/ghs/#_pict
#http://schoolscout24.de/cgi-bin/keminaco/hppinput.cgi
GHS_PICTOS = {
    'GHS01': {'icon-desc':'Exploding Bomb','desc':'Explosives',},
    'GHS02': {'icon-desc':'Flame','desc':'Flammables',},
    'GHS03': {'icon-desc':'Flame Over Circle','desc':'Oxidizers',},
    'GHS04': {'icon-desc':'Gas Cylinder','desc':'Compressed Gases',},
    'GHS05': {'icon-desc':'Corrosion','desc':'Corrosives',},
    'GHS06': {'icon-desc':'Skull and Crossbones','desc':'Acute Toxicity',},
    'GHS07': {'icon-desc':'Exclamation Mark','desc':'Irritant',},
    'GHS08': {'icon-desc':'Health Hazard','desc':'Health Hazard',},
    'GHS09': {'icon-desc':'Environment','desc':'Environment',},
}

def GHS_help_text(number):
    help_text= '<img HEIGHT="56" WIDTH="56" id="img_id_GHS0'+number+'" src="'+static('Hazard_Class_Pictograms/GHS0'+number+'.svg')+'" '
    help_text+= 'alt="'+GHS_PICTOS['GHS0'+number]['icon-desc']+'" title="'+GHS_PICTOS['GHS0'+number]['icon-desc']+'"><br>'
    return help_text

class HStatement(models.Model):
    code = models.CharField(max_length=10, unique=True)
    Hazard_statement = models.CharField(max_length=250)
    GHS01 = models.BooleanField(default=False, help_text=GHS_help_text('1')+GHS_PICTOS['GHS01']['desc'])#'Explosives')
    GHS02 = models.BooleanField(default=False, help_text=GHS_help_text('2')+GHS_PICTOS['GHS02']['desc'])#'Flammables')
    GHS03 = models.BooleanField(default=False, help_text=GHS_help_text('3')+GHS_PICTOS['GHS03']['desc'])#'Oxidizers')
    GHS04 = models.BooleanField(default=False, help_text=GHS_help_text('4')+GHS_PICTOS['GHS04']['desc'])#'Compressed Gases')
    GHS05 = models.BooleanField(default=False, help_text=GHS_help_text('5')+GHS_PICTOS['GHS05']['desc'])#'Corrosives')
    GHS06 = models.BooleanField(default=False, help_text=GHS_help_text('6')+GHS_PICTOS['GHS06']['desc'])#'Acute Toxicity')
    GHS07 = models.BooleanField(default=False, help_text=GHS_help_text('7')+GHS_PICTOS['GHS07']['desc'])#'Irritant')
    GHS08 = models.BooleanField(default=False, help_text=GHS_help_text('8')+GHS_PICTOS['GHS08']['desc'])#'Health Hazard')
    GHS09 = models.BooleanField(default=False, help_text=GHS_help_text('9')+GHS_PICTOS['GHS09']['desc'])#'Environment')
    def __str__(self):
        return self.code + ' ' + Truncator(self.Hazard_statement).chars(25)
    def pictos_list(self):
        list= ''
        for x in range(1, 10):
            number= str(x)
            if getattr(self, 'GHS0'+number):
                #list+= '<a href="'+reverse("admin:aspic2_hstatement_change", args=(self.pk,))+'">'
                list+= '<img HEIGHT="26" WIDTH="26" src="'+static('Hazard_Class_Pictograms/GHS0'+number+'.svg')+'" '
                list+= 'alt="'+GHS_PICTOS['GHS0'+number]['icon-desc']+'" title="'+GHS_PICTOS['GHS0'+number]['desc']+'">&nbsp;'
        return mark_safe(list)
    pictos_list.admin_order_field = Concat('GHS01', Value(' '), 'GHS02', Value(' '), 'GHS03', Value(' '), 'GHS04', Value(' '), 'GHS05', Value(' '), 'GHS06', Value(' '), 'GHS07', Value(' '), 'GHS08', Value(' '), 'GHS09')
    class Meta:
        #https://stackoverflow.com/questions/21578382/reorder-model-objects-in-django-admin-panel
        #verbose_name_plural or verbose_name + "s"   u"\u200B" : invisible zero-width spaces 
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "HStatement" #12-order  

class Provider(models.Model):
    name = models.CharField(unique=True, max_length=50)
    distributor = models.CharField(max_length=50)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "Provider" #11-order  
        ordering = ['name']

class Place(models.Model):
    code = models.CharField(unique=True, max_length=20, default='', blank=True)
    name = models.CharField(unique=True, max_length=50, default='', blank=True)
    RISKS= (
        ('HF', 'HF : réservé au HF (acide fluorhydrique)'),
        ('0',  ' 0 : interdit si risques spéciaux'),
        ('1',  ' 1 : réservé aux risques spéciaux'),
        ('2',  ' 2 : possible quelque soit le risque')
    )
    risk = models.CharField(max_length=5, default='', blank=True, choices= RISKS)
    def __str__(self):
        return self.name+' / '+self.risk
    def code_risk(self):
        #return self.code+' / '+self.risk
        return mark_safe('<a href="{}">{}</a>'.format(reverse("admin:aspic2_place_change", args=(self.pk,)), (self.code+' / '+self.risk) ))
    code_risk.admin_order_field = 'code'
    def barcode(self):
        return mark_safe('<svg id="barcode_place"></svg>')
    class Meta:
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "Place" #9-order 
        ordering = ['name']

class Borrow_place(models.Model):
    hallway = models.CharField(max_length=13, default='', blank=True)
    room = models.CharField(max_length=7, default='', blank=True)
    def __str__(self):
        return self.hallway+' / '+self.room
    class Meta:
        unique_together = ['hallway', 'room']
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "Borrow place" #10-order

class Keyword(models.Model):
    name = models.CharField(max_length=32, unique=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "Keyword" #13-order

def SDS_link_G(cas_number):
    if os.path.isfile(settings.SDS_PATH+cas_number+".pdf"):
        return mark_safe('<a href="{}">{}</a>'.format(
            settings.SDS_URL+cas_number+".pdf", 
            cas_number
        ))
    else: 
        return cas_number

def Product_Icon_cp(product):
    img_cp= '<img HEIGHT="14" WIDTH="14" src="'+static('icon_copy.svg')+'" '
    img_cp+= 'alt="copy" title="copy">'
    return mark_safe('<a href="javascript:navigator.clipboard.writeText(\'{}\')">{}</a>&nbsp'.format(
        product.name,
        img_cp
    ))
def Product_Link_G(product):
    return mark_safe('<a href="{}">{}</a>'.format(
        reverse("admin:aspic2_product_change", args=(product.pk,)),
        Truncator(product.name).chars(32)
    ))

class Product(models.Model):
    name = models.CharField(max_length=200)
    verif = models.BooleanField(default=False)
    # If you want to have the current date as default, use `django.utils.timezone.now
    #modified_date = models.DateTimeField('modified date', blank=True, default=timezone.now())
    # NameError: name 'django' is not defined
    #modified_date = models.DateTimeField('modified date', blank=True, default=django.utils.timezone.now)
    modified_date = models.DateTimeField('modified date', blank=True, default=timezone.now)
    # add unique=True for cas_number
    cas_number= models.CharField(max_length=32, blank=True, help_text='format : Y-nn-n or use an -X at the end')#, help_text='<span id="info_CAS_product"></span>')
    #django.db.utils.IntegrityError: (1062, "Duplicate entry '524-84-8' for key 'aspic_product_cas_number_c1c7eb9b_uniq'")
    #SELECT t.cas_number FROM aspic_product t GROUP BY t.cas_number HAVING COUNT(t.cas_number) > 1
    #065-00-X  10213-10-2  223437-05-6  26680-10-4   34369-07-8   524-84-8     7220-79-3    7440-62-2
    formula= models.CharField(max_length=255, blank=True)#default=None)
    hazard_statements = models.ManyToManyField(HStatement, blank=True)
    no_info= models.BooleanField(default=False)
    no_danger= models.BooleanField(default=False)
    Attention= models.BooleanField(default=False)
    H_ACID= models.BooleanField(default=False)
    H_BASE= models.BooleanField(default=False)
    H_AB= models.BooleanField('H_AB?', default=False)
    H_F= models.BooleanField(default=False)
    keywords = models.ManyToManyField(Keyword, blank=True)
    def Hazard_infos(self): return mark_safe('<span id="info_Check_hazard"></span>')
    def CAS_infos(self): return mark_safe('<span id="info_Check_CAS"></span>')
    def product_link(self): return Product_Link_G(self)
    def SDS_link(self):
        return SDS_link_G(self.cas_number)
    SDS_link.admin_order_field = 'cas_number'
    SDS_link.short_description = 'FDS link'
    def __str__(self):
        return Truncator(self.cas_number).chars(13) + ' | ' + Truncator(self.name).chars(32) 
    def other_infos(self):
        other_infos_str= ''
        if self.no_info: other_infos_str+= '<span style="color:orange">No info</span>, '
        if self.no_danger: other_infos_str+= '<span style="color:green">No danger</span>, '
        if self.Attention: other_infos_str+= '<span style="color:red">Attention</span>, '
        if self.H_ACID: other_infos_str+= 'H_ACID, '
        if self.H_BASE: other_infos_str+= 'H_BASE, '
        if self.H_AB: other_infos_str+= 'H_AB?, '
        if self.H_F: other_infos_str+= 'H_F, '
        other_infos_str= other_infos_str[:-2]
        return mark_safe(other_infos_str)
    other_infos.admin_order_field = Concat('no_info', Value(' '), 'no_danger', Value(' '))
    def get_hazard_statements(self):
        #return mark_safe(' '.join([('<span title="'+hs.Hazard_statement+'">'+hs.code+'</span>') for hs in self.hazard_statements.all()])) 
        list= ''; 
        for hs in self.hazard_statements.all():
            list+= '<span title="'+hs.Hazard_statement+'">'
            list+= '<a href="'+reverse("admin:aspic2_hstatement_change", args=(hs.pk,))+'">'+hs.code+'</a>'
            list+= '</span> '
        return mark_safe(list)
    get_hazard_statements.admin_order_field = 'hazard_statements'
    def get_hazard_statements_pictos(self):
        listOfCMR1 = ['H340' , 'H350', 'H350i', 'H360', 'H360D', 'H360Df', 'H360Fd', 'H360FD']
        listOfCMR2 = ['H341' , 'H351', 'H361', 'H361d', 'H361f', 'H361fd']
        listOfTplus = ['H300' , 'H310', 'H330']
        listOfPictos = [] 
        list= ''; CMR1= False; CMR2= False; Tplus= False;
        for hs in self.hazard_statements.all():
            hs_pictos_list= hs.pictos_list()
            #for picto in listOfPictos:
            if hs_pictos_list not in listOfPictos:
                listOfPictos.append(hs_pictos_list)
                list+= ' '+hs_pictos_list+''
            if hs.code in listOfCMR1: CMR1=True
            if hs.code in listOfCMR2: CMR2=True
            if hs.code in listOfTplus: Tplus=True
        if CMR1: list+= '<span style="color:red;font-size:120%">CMR1</span> '
        if CMR2: list+= '<span style="color:red;font-size:120%">CMR2</span> '
        if Tplus: list+= '<span style="color:red;font-size:120%">T+</span> '
        return mark_safe(list)
    get_hazard_statements_pictos.admin_order_field = 'hazard_statements'
    get_hazard_statements_pictos.short_description = 'Hazard Pictograms'
    class Meta:
        ordering = ['-modified_date']
        #ordering = ['-name']#['-modified_date']
        verbose_name_plural = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "Product" #8-order 

class Abstract_Demand(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT, null=True, help_text='<span id="info_product"></span>')
    complement = models.CharField(blank=True, max_length=48, default='', help_text="additional information on the product name and number") #complément
    pureness = models.CharField(blank=True, max_length=10, default='') #pureté
    capacity = models.CharField(max_length=16, default='', help_text="ex. : 100 ml , 50 g") #contenance blank=True, 
    #Etat physique   Non renseigné   Solide   Liquide
    PHYSICAL_STATE = ( ('Unspecified', 'Unspecified'), ('Solid', 'Solid'), ('Liquid', 'Liquid') ) 
    physical_state = models.CharField(blank=True, max_length=13, default='Unspecified', choices= PHYSICAL_STATE)
    #Conditionnement    Non renseigné    Verre   Métal   Plastique
    CONDITIONING = ( ('Unspecified', 'Unspecified'), ('Glass', 'Glass'), ('Metal', 'Metal'), ('Plastic', 'Plastic') )
    conditioning = models.CharField(blank=True, max_length=13, default='Unspecified', choices= CONDITIONING)
    reference = models.CharField(max_length=32, default='') #reference catalogue 
    num_order = models.CharField(blank=True, max_length=17, default='')#numero_bon_commande
    #P.U. HT     Utiliser le "POINT" decimal separateur, ex.: 15.99, b4 unit_price_excl_tax
    uprice = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    demandeur = models.CharField(max_length=24, default='')
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT, null=True)
    place = models.ForeignKey(Place, on_delete=models.PROTECT, null=True)
    # it seems not recommanded to use auto_now_add nor auto_now
    date_modified = models.DateTimeField('modified date', null=True, blank=True, default=timezone.now)#commandes_date_maj
    date_demanded = models.DateField('demanded date', null=True, blank=True, default=timezone.now)#commandes_date_demande
    date_commanded = models.DateField('commanded date', null=True, blank=True)#commandes_date_commande
    # to move to Abstract_Container ?
    date_received = models.DateField('received date', null=True, blank=True)#commandes_date_recu
    def __str__(self):
        return self.product.name
    def place_risk(self): 
        if self.place is None: return ''
        else: return self.place.code_risk()
    place_risk.admin_order_field = 'place__code'
    def other_infos(self): return self.product.other_infos()
    other_infos.admin_order_field = 'product__other_infos'
    def get_hazard_statements_pictos(self): return self.product.get_hazard_statements_pictos()
    get_hazard_statements_pictos.admin_order_field = 'product__hazard_statements'
    def cas_number(self): return self.product.cas_number
    cas_number.admin_order_field = 'product__cas_number'
    def name(self): return Truncator(self.product.name).chars(32)
    name.admin_order_field = 'product__name'
    def Cname(self): return self.product.name
    Cname.admin_order_field = 'product__name'
    def product_link(self): return Product_Link_G(self.product)
    product_link.admin_order_field = 'product__name'
    product_link.short_description = 'product'
    def product_icon_link(self): return Product_Icon_cp(self.product) + Product_Link_G(self.product)
    product_icon_link.admin_order_field = 'product__name'
    product_icon_link.short_description = 'product'
    def SDS_link_C(self):
        return SDS_link_G(self.product.cas_number)
    SDS_link_C.admin_order_field = 'product__cas_number'
    SDS_link_C.short_description = 'CAS_Number'
    def __str__(self):
        return self.product.name
    class Meta:
        abstract = True

class Demand(Abstract_Demand):
    quantity = models.PositiveIntegerField(default=1) 
    def total_price_ET(self): return round( float(self.uprice) * int(self.quantity) , 2 )
    def distributor(self): return self.provider.distributor if self.provider is not None else ''
    class Meta():
        ordering = ['provider']
        verbose_name = u"\u200B\u200B\u200B\u200B" + "Demand" #4-order 
        verbose_name_plural = u"\u200B\u200B\u200B\u200B" + "Demand" #4-order 

class DemandCheck(Demand):
    class Meta:
        proxy = True
        ordering = ['date_demanded']
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B" + "Demand check" #5-order 
        verbose_name_plural = u"\u200B\u200B\u200B\u200B\u200B" + "Demand check" #5-order 

class Command(Abstract_Demand):
    quantity = models.PositiveIntegerField(default=1)
    class Meta():
        ordering = ['num_order']
        verbose_name = u"\u200B\u200B\u200B\u200B\u200B\u200B" + "Command" #6-order 

class Abstract_Container(Abstract_Demand):
    in_stock= models.BooleanField(default=True)
    borrow_place= models.ForeignKey(Borrow_place, on_delete=models.PROTECT, blank=True, null=True)
    borrower = models.CharField(max_length=48, default='', blank=True)
    def stock_loc(self):
        if self.in_stock==True: return 'IN'
        else: 
            borrow_loc= self.borrower
            try:
                borrow_loc+= ' : '+str(self.borrow_place)
            except ObjectDoesNotExist:
                pass
            return borrow_loc
    stock_loc.admin_order_field = Concat('borrower', Value(' '), 'borrow_place__hallway', Value(' '), 'borrow_place__room', Value(' '))
    #pictos_list.admin_order_field = Concat('GHS01', Value(' '), 'GHS02', Value(' '), 'GHS03', Value(' '), 'GHS04', Value(' '), 'GHS05', Value(' '), 'GHS06', Value(' '), 'GHS07', Value(' '), 'GHS08', Value(' '), 'GHS09')
    def get_hazard_statements_pictos(self): return self.product.get_hazard_statements_pictos()
    get_hazard_statements_pictos.admin_order_field = 'product__hazard_statements'
    def Cname(self): return self.product.name
    Cname.admin_order_field = 'product__name'
    class Meta:
        abstract = True

class Container_Base(Abstract_Container):
    code = models.PositiveIntegerField(unique=True, null=True, help_text='<svg id="barcode_container"></svg>')
    def container_link(self):
        return mark_safe('<a href="{}">{}</a>'.format(
        reverse("admin:aspic2_container_change", args=(self.pk,)),
        self.name()
        ))
    class Meta():
        ordering = ['-date_modified']
        verbose_name = u"\u200B" + "Container_Base" #1-order 
        verbose_name_plural = u"\u200B" + "Container_Base" #1-order 
        get_latest_by = 'code'

class Container_Suppr(Abstract_Container):
    code = models.PositiveIntegerField(null=True, help_text='<svg id="barcode_container"></svg>')
    class Meta(Abstract_Demand.Meta):
        ordering = ['-date_modified']
        verbose_name_plural = u"\u200B\u200B\u200B\u200B\u200B\u200B\u200B" + "Suppressed" #7-order

#class Container_View1(Container):
#    class Meta:
#        proxy = True
#        verbose_name_plural = u"\u200B\u200B" + "Container_2old" #2-order 

class Container(Container_Base):
    class Meta:
        proxy = True
        verbose_name = u"\u200B\u200B\u200B" + "Container" #3-order 
        verbose_name_plural = u"\u200B\u200B\u200B" + "Container" #3-order 

