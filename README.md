# ASPiC2_Demo

This is a demonstration version of ASPiC2 developped by [LCMCP](https://lcmcp.sorbonne-universite.fr) under license GPL v3.

LCMCP is a chemistry lab of [Sorbonne Université](https://www.sorbonne-universite.fr) and [CNRS](https://www.cnrs.fr). 



ASPiC2_Demo is an example use of ASPiC2 with randomly generated data.



ASPiC2 is a Django project, it aims to reference all the chemicals present in a lab.
To adapt it for your lab you will probably have to modify the code.
We use a MariaDB database for historical reasons.

To run it, you have to configure it with your database access, domain name and static folder path.
Once running, you will probably define your own storage locations, providers and products.
This demo comes with imaginary data.

ASPiC2 tries to factorize the code using inherited models which follow the life cycle of a container.
Here is a simplified diagram of the models :

````
Abstract_Demand (has the Product attribute)
       |
       +-------> Demand  (become a Command with "command" action)
       |
       +-------> Command (become a Container with "receive" action)
       |
       +-------> Abstract_Container
                        |
                        +----------> Container_Base  (become a Container_Suppr with "supress" action, has "borrow" and "return" actions)
                        |                    |
                        |                    +-----> Container (proxy model for possible other views)
                        |
                        +----------> Container_Suppr (become a Container with "restore" action) 
````

ASPiC2 is designed to be used with only two users (not counting the superuser).

Basically it starts with the user "borrower" which add a "Demand" to ask for a new chemicals. 
 
Each container has a corresponding chemical product.
Several containers can therefore correspond to the same product, for example there may be several bottles of ethanol.
Each container has a bar code, which is its unique identification number, for example if there are 5 bottles of ethanol they will each have a different bar code.
Each product has a unique CAS number.

Then the "manager" user can "command" (or order) the "Demand" which became a "Command".
When the chemicals are delivered, he can "received" the "Command" which became one or more "Containers" with unique bar codes.

The user "borrower" can "borrow", "return", "suppress" and "restore" containers.

