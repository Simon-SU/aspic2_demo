from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
import logging
import json
from .models import Product, Container_Base, Command, Demand, Product , Container_Suppr
from .admin import recycle_codes
from django.views.generic import ListView
from django.db.models import Avg, Count, Min, Sum
from django.db.models import Q
from datetime import date
#from babel.numbers import format_decimal
from django.contrib.auth.decorators import login_required, user_passes_test
from pathlib import Path
from django.conf import settings

@login_required
def get_last_generated_csv(request):
    #https://stackoverflow.com/questions/50479804/how-to-send-file-to-response-in-django
    #filename= ''
    for p in Path(settings.PROJECT_PATH).glob("last*.csv"):
        filename= p.name 
        logging.debug(' p.name : ' + p.name)
    file_location= settings.PROJECT_PATH + filename
    #file_location= settings.PROJECT_PATH 
    #file_location+= filename
    #file_location= '/home/user/aspic2_project/aspic2_project/last.csv'
    try:    
        with open(file_location, 'r') as f:
           file_data = f.read()
        response = HttpResponse(file_data, content_type='text/csv')
        #response['Content-Disposition'] = 'attachment; filename="last.csv"'
        response['Content-Disposition'] = 'attachment; filename="'+filename+'"'
    except IOError:
        response = HttpResponseNotFound('<h1>File not exist</h1>')
    return response    

@login_required
#@user_passes_test(lambda u: u.groups.filter(name='manager').exists())
#@user_passes_test(lambda u: u.groups.filter(name='manager').exists() or u.is_superuser) # u.users.filter(name='djaspic').exists())
def board(request):
    recycle_codes_json = recycle_codes()
    Count_Container_Base= Container_Base.objects.count()
    Count_Container_Suppr= Container_Suppr.objects.count()
    Count_Product= Product.objects.count()
    Count_Demand= Demand.objects.count()
    Count_Command= Command.objects.count()
    # Nb. Produits differents en stock
    Container_Prod_Distinct= Container_Base.objects.aggregate(Count('product', distinct=True))
    Count_Container_Prod_Distinct= Container_Prod_Distinct['product__count']
    # CMR  # https://stackoverflow.com/questions/5956391/django-objects-filter-with-list
    listOfCMR1 = ['H340' , 'H350', 'H350i', 'H360', 'H360D', 'H360Df', 'H360Fd', 'H360FD']
    listOfCMR2 = ['H341' , 'H351', 'H361', 'H361d', 'H361f', 'H361fd']
    listOfCMR= listOfCMR1 + listOfCMR2
    CMR_filter = Q()
    for code in listOfCMR:
        CMR_filter = CMR_filter | Q(product__hazard_statements__code=code)
    Count_Container_CMR= Container_Base.objects.filter(CMR_filter).count()
    # Tplus 
    listOfTplus = ['H300' , 'H310', 'H330']
    Tplus_filter = Q()
    for code in listOfTplus:
        Tplus_filter = Tplus_filter | Q(product__hazard_statements__code=code)
    Count_Container_Tplus= Container_Base.objects.filter(Tplus_filter).count()
    # Explosives  
    Count_Container_Explod= Container_Base.objects.filter(product__hazard_statements__GHS01=True).count()
    # Flammables
    Count_Container_Flame= Container_Base.objects.filter(product__hazard_statements__GHS02=True).count()
    # Achats année dernière
    lastyear= date.today().year - 1
    Res_Sum_Container_LastYear= Container_Base.objects.filter(date_received__year=lastyear).aggregate(Sum('uprice'))
    Sum_Container_LastYear= Res_Sum_Container_LastYear['uprice__sum']
    Res_Sum_Container_Suppr_LastYear= Container_Suppr.objects.filter(date_received__year=lastyear).aggregate(Sum('uprice'))
    Sum_Container_Suppr_LastYear= Res_Sum_Container_Suppr_LastYear['uprice__sum']
    Sum_Container_All_LastYear= Sum_Container_LastYear + Sum_Container_Suppr_LastYear
    #https://stackoverflow.com/questions/37579712/format-numbers-as-currency-in-python
    #Sum_Container_All_LastYear= format_currency(Sum_Container_All_LastYear, 'EUR', locale='de_DE')#fr_FR.utf8
    # Achats Sesmestre 1   et 2  
    semester1a = date(date.today().year, 1, 1)
    semester1b = date(date.today().year, 6, 30)
    Res_Sum_Container_Sem1= Container_Base.objects.filter(date_received__gte=semester1a, date_received__lte=semester1b).aggregate(Sum('uprice'))
    if Res_Sum_Container_Sem1['uprice__sum'] is not None:
        Sum_Container_Sem1= Res_Sum_Container_Sem1['uprice__sum']
    else : 
        Sum_Container_Sem1= 0
    Res_Sum_Container_Suppr_Sem1= Container_Suppr.objects.filter(date_received__gte=semester1a, date_received__lte=semester1b).aggregate(Sum('uprice'))
    if Res_Sum_Container_Suppr_Sem1['uprice__sum'] is not None:
        Sum_Container_Suppr_Sem1= Res_Sum_Container_Suppr_Sem1['uprice__sum']
    else : 
        Sum_Container_Suppr_Sem1= 0
    logging.debug(' Sum_Container_Sem1 : ' + str(Sum_Container_Sem1))
    Sum_Container_All_Sem1= Sum_Container_Sem1 + Sum_Container_Suppr_Sem1
    semester2a = date(date.today().year, 7, 1)
    semester2b = date(date.today().year, 12, 31)
    Res_Sum_Container_Sem2= Container_Base.objects.filter(date_received__gte=semester2a, date_received__lte=semester2b).aggregate(Sum('uprice'))
    #logging.debug(' Res_Sum_Container_Sem2[\'uprice__sum\'] : ' + str(Res_Sum_Container_Sem2['uprice__sum']))
    if Res_Sum_Container_Sem2['uprice__sum'] is not None:
        Sum_Container_Sem2= Res_Sum_Container_Sem2['uprice__sum']
    else : 
        Sum_Container_Sem2= 0
    Res_Sum_Container_Suppr_Sem2= Container_Suppr.objects.filter(date_received__gte=semester2a, date_received__lte=semester2b).aggregate(Sum('uprice'))
    if Res_Sum_Container_Suppr_Sem2['uprice__sum'] is not None:
        Sum_Container_Suppr_Sem2= Res_Sum_Container_Suppr_Sem2['uprice__sum']
    else : 
        Sum_Container_Suppr_Sem2= 0
    #logging.debug(' Sum_Container_Sem2 : ' + str(Sum_Container_Sem2))
    #Sum_Container_All_Sem2= 0
    Sum_Container_All_Sem2= Sum_Container_Sem2 + Sum_Container_Suppr_Sem2
    #logging.debug(' Sum_Container_LastYear : ' + repr(Sum_Container_LastYear))
    context = {'max_code': Container_Base.objects.latest().code, 'recycle_codes': recycle_codes_json, \
              'Count_Container_Base': Count_Container_Base, \
              'Count_Container_Suppr': Count_Container_Suppr, \
              'Count_Product': Count_Product, \
              'Count_Demand': Count_Demand, \
              'Count_Command': Count_Command, \
              'Count_Container_Prod_Distinct': Count_Container_Prod_Distinct, \
              'Count_Container_CMR': Count_Container_CMR, \
              'Count_Container_Tplus': Count_Container_Tplus, \
              'Count_Container_Explod': Count_Container_Explod, \
              'Count_Container_Flame': Count_Container_Flame, \
              'lastyear': lastyear, \
              'Sum_Container_All_LastYear': Sum_Container_All_LastYear, \
              'Sum_Container_All_Sem1': Sum_Container_All_Sem1, \
              'Sum_Container_All_Sem2': Sum_Container_All_Sem2, \
              }
    return render(request, 'board.html', context)

def about(request):
    return render(request, 'about.html')

@login_required
def help(request):
    return render(request, 'help.html')

def aide(request):
    return render(request, 'aide.html')

def verif_duplicate(request):
    cas_number = request.GET.get('cas_number','') 
    queryset_verif_duplicate = Product.objects.filter(cas_number=cas_number)
    count_qs= queryset_verif_duplicate.count()
    result = []
    result.append({'cas_number_count':count_qs})
    return HttpResponse(json.dumps(result), content_type="application/json") 

def get_product_links_hstatement(request): 
    #logging.debug(' id : ' + str(request.GET.get('id','')))
    id = request.GET.get('id','') 
    p = Product.objects.get(id=int(id))
    #logging.debug(' p get_hazard_statements_pictos  : ' + str(p.get_hazard_statements_pictos()))
    result = []
    result.append({'get_hazard_statements_pictos':str(p.get_hazard_statements_pictos()), 'SDS_link':str(p.SDS_link()), 'formula':str(p.formula), 'product_link':str(p.product_link())})
    return HttpResponse(json.dumps(result), content_type="application/json") 

def get_container_by_code(request):
    #https://docs.djangoproject.com/en/3.1/topics/http/views/
    #https://domain.org/admin/aspic2/get_container_by_code/?code=13531
    code = request.GET.get('code','') 
    c = Container_Base.objects.get(code=code)
    return HttpResponseRedirect('/admin/aspic2/container/%s/' % c.id)

# not used
#class ContainerSearchListView(ListView):
#    model = Container
#    paginate_by = 100
#    template_name='admin/change_list_container_search.html'
#    # ordering = ['-created']
#    place_code= ''; borrow_place_hallway= ''; borrow_place_room= ''
#    def get_queryset(self):
#        if self.request.GET.get("place_code") is not None:
#            self.place_code= self.request.GET.get("place_code")
#            self.borrow_place_hallway= self.request.GET.get("borrow_place_hallway")
#            self.borrow_place_room= self.request.GET.get("borrow_place_room")
#            logging.debug('-place_code-' + self.place_code + '-borrow_place_hallway-' + self.borrow_place_hallway + '-borrow_place_room-' + self.borrow_place_room + '-')
#        else :
#            logging.debug('-place_code is None !?!-')
#        search_results= Container.objects.all()
#        if self.place_code!='': search_results= search_results.filter(place__code__icontains=self.place_code)
#        if self.borrow_place_hallway!='': search_results= search_results.filter(borrow_place__hallway__icontains=self.borrow_place_hallway)
#        if self.borrow_place_room!='': search_results= search_results.filter(borrow_place__room__icontains=self.borrow_place_room)
#        return search_results
#    def get_context_data(self, **kwargs):
#        context = super().get_context_data(**kwargs)
#        context['place_code'] = self.place_code; context['borrow_place_hallway'] = self.borrow_place_hallway; context['borrow_place_room'] = self.borrow_place_room
#        return context

