from django.db import connection
from django.db import connections
from collections import namedtuple
from django.utils import timezone

from aspic2.models import HStatement, Provider, Place, Borrow_place, Product, Keyword, Demand, Command, Container_Base, Container_Suppr
import time, datetime, json, re

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count

class Essais():
    debug= ''
    debug1= ''

    def es_distinct():
        # Nb. Produits differents en stock
        Container_Prod_Distinct= Container_Base.objects.aggregate(Count('product', distinct=True))
        # Container_Prod_Distinct= Container_Base.objects.annotate(Count('product', distinct=True))
        Essais.debug1+= repr(Container_Prod_Distinct)
        # -{'product__count': 2254}-
        Essais.debug1+= ' '+ str(Container_Prod_Distinct['product__count'])
 
    def doessais():
        "Essais ASPiC2"
        start_essais = time.time()

        Essais.es_distinct()

        end_essais = time.time()
        return Essais.debug+ '-'  + Essais.debug1 +   '-' + "Essais ASPiC2 in " + str(int(end_essais - start_essais)) + " secondes"



