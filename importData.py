from django.db import connection
from django.db import connections
from collections import namedtuple
from django.utils import timezone

from aspic2.models import HStatement, Provider, Place, Borrow_place, Product, Keyword, Demand, Command, Container_Base, Container_Suppr

import time, json, re, random, string
from datetime import datetime, timedelta

from random import randrange

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count

from django.contrib.auth.models import Group, User, Permission
from django.contrib.contenttypes.models import ContentType

class ImportData():
    debug= ''
    debug_HStatement_import= ''
    debug_Keyword_import= ''
    debug_Product_Hstatement_import= ''
    debug_random_containers= ''
    debug_random_containers_Suppr= ''

    def provider_import():
        with open('aspic2/static/aspic2_provider.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_provider_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                aprovider= Provider(id= kw_data['id'], name= kw_data['name'], distributor= kw_data['distributor'])
                aprovider.save()

    def borrow_place_import():
        with open('aspic2/static/aspic2_borrow_place.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_borrow_place_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                aBorrow_place= Borrow_place(id= kw_data['id'], hallway= kw_data['hallway'], room= kw_data['room'])
                aBorrow_place.save()

    def place_import():
        with open('aspic2/static/aspic2_place.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_place_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                aplace= Place(id= kw_data['id'], code= kw_data['code'], name= kw_data['name'], risk= kw_data['risk'])
                aplace.save()

    def HStatement_import():
        with open('aspic2/static/aspic2_hstatement.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_Keyword_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                aKeyword= HStatement(id= kw_data['id'], code= kw_data['code'], Hazard_statement= kw_data['Hazard_statement'], GHS01= kw_data['GHS01'], GHS02= kw_data['GHS02'], GHS03= kw_data['GHS03'], GHS04= kw_data['GHS04'], GHS05= kw_data['GHS05'], GHS06= kw_data['GHS06'], GHS07= kw_data['GHS07'], GHS08= kw_data['GHS08'], GHS09= kw_data['GHS09'])
                aKeyword.save()

    def Keyword_import():
        with open('aspic2/static/aspic2_keyword.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_Keyword_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                aKeyword= Keyword(id= kw_data['id'], name= kw_data['name'])
                aKeyword.save()

    def Product_import():
        with open('aspic2/static/aspic2_product.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_Keyword_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                aProduct= Product(id= kw_data['id'], name= kw_data['name'], verif= kw_data['verif'], modified_date= kw_data['modified_date'], cas_number= kw_data['cas_number'], formula= kw_data['formula'], no_info= kw_data['no_info'], no_danger= kw_data['no_danger'], Attention= kw_data['Attention'], H_ACID= kw_data['H_ACID'], H_BASE= kw_data['H_BASE'], H_AB= kw_data['H_AB'], H_F= kw_data['H_F'])
                aProduct.save()

    def Product_Hstatement_import():
        with open('aspic2/static/aspic2_product_hazard_statements.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_Product_Hstatement_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                try:
                    aProduct= Product.objects.get(id=kw_data['product_id'])
                except ObjectDoesNotExist:
                    #ImportData.debug_Product_Hstatement_import+= ' no Product id : ' + str(kw_data['product_id'])
                    continue
                try:
                    aHStatement= HStatement.objects.get(id=kw_data['hstatement_id'])
                except ObjectDoesNotExist:
                    #ImportData.debug_Product_Hstatement_import+= ' no Hstatement id : ' + str(kw_data['hstatement_id'])
                    continue
                aProduct.hazard_statements.add(aHStatement)
                aProduct.save()

    def Product_Keyword_import():
        with open('aspic2/static/aspic2_product_keywords.json') as json_file:
            data = json.load(json_file)
            #ImportData.debug_Product_Hstatement_import+= str(data[2]['data'])
            for kw_data in data[2]['data']:
                try:
                    aProduct= Product.objects.get(id=kw_data['product_id'])
                except ObjectDoesNotExist:
                    #ImportData.debug_Product_Hstatement_import+= ' no Product id : ' + str(kw_data['product_id'])
                    continue
                try:
                    #aKeyword= Keyword(id= kw_data['id'], name= kw_data['name'])
                    aKeyword= Keyword.objects.get(id=kw_data['keyword_id'])
                except ObjectDoesNotExist:
                    #ImportData.debug_Product_Hstatement_import+= ' no Hstatement id : ' + str(kw_data['hstatement_id'])
                    continue
                aProduct.keywords.add(aKeyword)
                aProduct.save()

    def groupperms_create():
        manager_group, created= Group.objects.get_or_create(name='manager')
        admin_user = User.objects.create_user(username='director', password='*******')
        admin_user.groups.add(manager_group)
        admin_user.is_staff=True 
        admin_user.save()
        borrower_group, created= Group.objects.get_or_create(name='borrower')
        borrower_user = User.objects.create_user(username='labo', password='********')
        borrower_user.groups.add(borrower_group)
        borrower_user.is_staff=True 
        borrower_user.save()
        permissions= {}
        manager_perm= {'view_logentry', 'view_place', 'view_hstatement', 'add_demand', 'change_demand', 'delete_demand', 'view_demand', 'add_keyword', 'view_keyword', 'change_command', 'delete_command', 'view_command', 'add_product', 'change_product', 'delete_product', 'view_product', 'view_container_suppr', 'add_borrow_place', 'view_borrow_place', 'add_provider', 'change_provider', 'delete_provider', 'view_provider', 'add_container', 'change_container', 'view_container'}
        borrower_perm= {'view_logentry', 'view_place', 'view_hstatement', 'add_demand', 'view_demand', 'add_keyword', 'view_keyword', 'view_command', 'add_product', 'view_product', 'view_container_suppr', 'add_borrow_place', 'view_borrow_place', 'view_provider', 'add_container', 'view_container'}
        for perm_code in manager_perm: 
            manager_group.permissions.add(Permission.objects.get(codename=perm_code))
        for perm_code in borrower_perm: 
            borrower_group.permissions.add(Permission.objects.get(codename=perm_code))

    pureness_eg= ['', '', '', '', '', '>90%', '96%', '70%', '100%']
    PHYSICAL_STATE = ['Unspecified', 'Solid', 'Liquid'] 
    CONDITIONING = ['Unspecified', 'Glass', 'Metal', 'Plastic']
    people= [ 'Kristi', 'Jorge', 'Scott', 'Peter', 'Teresa', 'Aurélie', 'Emmanuel', 'Maggie', 'Éric', 'Nicolai', 'Bianka', 'Magnus', 'Gianfranco', 'Gianna', 'Iolanda', 'Julia', 'João', 'Vitor', 'Кулагина', 'Аверкий', 'José', 'Marcio', 'Delfina', 'Özakan', 'Abdülahat', 'İhsanoğlu', '浩然', '宇航', '语汐', '梦瑶', '依诺']
    Capacity_eg=  ['100 ml', '50 g', '5L', '2g', '2kg', '10cl']

    def random_commands():
        Count_Product= Product.objects.count()
        Count_Place= Place.objects.count()
        Count_Provider= Provider.objects.count()
        for ci in range(40):
            while True:
                aProviderId= randrange(Count_Provider)
                try: aProvider= Provider.objects.get(id= aProviderId)
                except ObjectDoesNotExist: continue
                break
            while True:
                aProdId= randrange(Count_Product)
                try: aProduct= Product.objects.get(id= aProdId)
                except ObjectDoesNotExist: continue
                break
            while True:
                aPlaceId= randrange(Count_Place)
                try: aPlace= Place.objects.get(id= aPlaceId)
                except ObjectDoesNotExist: continue
                break
            start_date= datetime.now() - timedelta(weeks=480) # 10 years
            end_date= datetime.now() - timedelta(weeks=1) 
            date_demanded= start_date + random.random()*(end_date - start_date)
            date_commanded= date_demanded + random.random()*timedelta(weeks=10)
            if date_commanded > datetime.now() : date_commanded = datetime.now()
            date_modified= date_commanded
            command= Command(product=aProduct, place= aPlace,  
                    provider= aProvider, pureness=random.choice(ImportData.pureness_eg), 
                    physical_state= random.choice(ImportData.PHYSICAL_STATE), conditioning= random.choice(ImportData.CONDITIONING), 
                    demandeur= random.choice(ImportData.people), capacity=  random.choice(ImportData.Capacity_eg), 
                    uprice= round(random.uniform(5.5, 201.9),2), quantity= randrange(2)+1,  
                    reference= ''.join(random.choices(string.ascii_uppercase + string.digits, k=5)),
                    num_order= ''.join(random.choices(string.ascii_uppercase + string.digits, k=5)),
                    date_demanded= date_demanded, date_commanded= date_commanded, date_modified= date_modified)
            command.save()

    def random_demands():
        Count_Product= Product.objects.count()
        Count_Place= Place.objects.count()
        Count_Provider= Provider.objects.count()
        for ci in range(40):
            while True:
                aProviderId= randrange(Count_Provider)
                try: aProvider= Provider.objects.get(id= aProviderId)
                except ObjectDoesNotExist: continue
                break
            while True:
                aProdId= randrange(Count_Product)
                try: aProduct= Product.objects.get(id= aProdId)
                except ObjectDoesNotExist: continue
                break
            while True:
                aPlaceId= randrange(Count_Place)
                try: aPlace= Place.objects.get(id= aPlaceId)
                except ObjectDoesNotExist: continue
                break
            start_date= datetime.now() - timedelta(weeks=480) # 10 years
            end_date= datetime.now() - timedelta(weeks=1) 
            date_demanded= start_date + random.random()*(end_date - start_date)
            date_modified= date_demanded
            demand= Demand(product=aProduct, place= aPlace,  
                    provider= aProvider, pureness=random.choice(ImportData.pureness_eg), 
                    physical_state= random.choice(ImportData.PHYSICAL_STATE), conditioning= random.choice(ImportData.CONDITIONING), 
                    demandeur= random.choice(ImportData.people), capacity=  random.choice(ImportData.Capacity_eg), 
                    uprice= round(random.uniform(5.5, 201.9),2), quantity= randrange(2)+1,  
                    reference= ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))
                    , date_demanded= date_demanded, date_modified= date_modified)
            demand.save()

    def random_containers():
        Count_Product= Product.objects.count()
        Count_Place= Place.objects.count()
        Count_Provider= Provider.objects.count()
        Count_Borrow_place= Borrow_place.objects.count()
        for ci in range(13000):
            #ImportData.debug_random_containers+= 'physical state: ' + random.choice(ImportData.PHYSICAL_STATE) + 'conditioning: ' + random.choice(ImportData.CONDITIONING)
            while True:
                aProviderId= randrange(Count_Provider)
                try: aProvider= Provider.objects.get(id= aProviderId)
                except ObjectDoesNotExist: continue
                break
            while True:
                aProdId= randrange(Count_Product)
                try:
                    aProduct= Product.objects.get(id= aProdId)
                except ObjectDoesNotExist:
                    #ImportData.debug_random_containers+= ' no Product id : ' + str(aProdId)
                    continue
                #ImportData.debug_random_containers+= ' found Product id : ' + str(aProdId)
                break
            while True:
                aPlaceId= randrange(Count_Place)
                try:
                    aPlace= Place.objects.get(id= aPlaceId)
                except ObjectDoesNotExist:
                    #ImportData.debug_random_containers+= ' no Place id : ' + str(aPlaceId)
                    continue
                #ImportData.debug_random_containers+= ' found Place id : ' + str(aPlaceId)
                break
            while True:
                aBorrow_placeId= randrange(Count_Borrow_place)
                try: aBorrow_place= Borrow_place.objects.get(id= aBorrow_placeId)
                except ObjectDoesNotExist: continue
                break
            start_date= datetime.now() - timedelta(weeks=480) # 10 years
            end_date= datetime.now() - timedelta(weeks=1) 
            date_demanded= start_date + random.random()*(end_date - start_date)
            date_commanded= date_demanded + random.random()*timedelta(weeks=10)
            if date_commanded > datetime.now() : date_commanded = datetime.now()
            date_received= date_commanded + random.random()*timedelta(weeks=10)
            if date_received > datetime.now() : date_received = datetime.now()
            date_modified= date_received + random.random()*(datetime.now() - date_received)
            in_stock= True
            borrower= '' 
            theBorrow_place= None
            if random.random()<0.15 : 
                borrower= random.choice(ImportData.people)
                in_stock= False
                theBorrow_place= aBorrow_place
            container= Container_Base(code= ci, product=aProduct, place= aPlace, borrower= borrower,  
                    provider= aProvider, in_stock= in_stock, pureness=random.choice(ImportData.pureness_eg), 
                    physical_state= random.choice(ImportData.PHYSICAL_STATE), conditioning= random.choice(ImportData.CONDITIONING), 
                    demandeur= random.choice(ImportData.people), capacity=  random.choice(ImportData.Capacity_eg), 
                    uprice= round(random.uniform(5.5, 201.9),2), 
                    reference= ''.join(random.choices(string.ascii_uppercase + string.digits, k=5)),
                    num_order= ''.join(random.choices(string.ascii_uppercase + string.digits, k=5)),
                    date_demanded= date_demanded, date_commanded= date_commanded, date_received= date_received, date_modified= date_modified, 
                    borrow_place= theBorrow_place)
            container.save()

    def random_containers_Suppr():
        Count_Container= Container_Base.objects.count()
        nb_Suppr= int(Count_Container*2/3) 
        ImportData.debug_random_containers_Suppr+= ' Count_Container : ' + str(Count_Container) + ' nb_Suppr: ' + str(nb_Suppr)
        for ci in range(nb_Suppr):
            while True:
                aContainerId= randrange(Count_Container)
                try: aContainer= Container_Base.objects.get(code= aContainerId)
                except ObjectDoesNotExist: continue
                break
            date_modified= aContainer.date_modified + random.random()*(datetime.now() - aContainer.date_modified)
            container_suppr= Container_Suppr(product=aContainer.product, demandeur=aContainer.demandeur, place=aContainer.place, 
                    uprice=aContainer.uprice, physical_state=aContainer.physical_state, conditioning=aContainer.conditioning, 
                    provider=aContainer.provider, reference=aContainer.reference, 
                    pureness=aContainer.pureness, capacity=aContainer.capacity, 
                    code=aContainer.code, in_stock=aContainer.in_stock, borrow_place=aContainer.borrow_place, 
                    borrower=aContainer.borrower,
                    date_demanded=aContainer.date_demanded, date_commanded= aContainer.date_commanded, 
                    date_received=aContainer.date_received, date_modified= date_modified)
            container_suppr.save()
            aContainer.delete()
       
    def doImportData():
        "Import example data to ASPiC2-Django"
        start_import = time.time()

        #ImportData.groupperms_create()

        #Provider.objects.all().delete()
        #ImportData.provider_import()

        #Borrow_place.objects.all().delete()
        #ImportData.borrow_place_import()

        #Place.objects.all().delete()
        #ImportData.place_import()

        #HStatement.objects.all().delete()
        #ImportData.HStatement_import()

        #Keyword.objects.all().delete()
        #ImportData.Keyword_import()

        #Product.objects.all().delete()
        #ImportData.Product_import()

        #ImportData.Product_Hstatement_import()
        #ImportData.Product_Keyword_import()


        #Container_Base.objects.all().delete()
        #ImportData.random_containers()
        #Container_Suppr.objects.all().delete()
        #ImportData.random_containers_Suppr()

        #Demand.objects.all().delete()
        #ImportData.random_demands()

        #Command.objects.all().delete()
        #ImportData.random_commands()

        end_import = time.time()

        ImportData.debug+= '-' + ImportData.debug_HStatement_import
        ImportData.debug+= '-' + ImportData.debug_Keyword_import
        ImportData.debug+= '-' + ImportData.debug_Product_Hstatement_import
        ImportData.debug+= '-' + ImportData.debug_random_containers
        ImportData.debug+= '-' + ImportData.debug_random_containers_Suppr
        #ImportData.debug+= '-' + ImportData.debug_Product_0_Keyword + '-'+ImportData.debug_Containers_0_Borrow_place+ '-'+ImportData.debug_Suppressed_Place 
        ImportData.debug+= '-' + "Import example data to ASPiC2-Django in " + str(int(end_import - start_import)) + " secondes"

        return  ImportData.debug 







#python manage.py shell
#from aspic2.importData import ImportData
#ImportData.doImportData()


