"""aspic2 URL Configuration
"""
from django.urls import path
#from django.urls import re_path
from . import views
#from aspic2.views import ContainerSearchListView

urlpatterns = [
    path('board/', views.board),
    path('about/', views.about),
    path('help/', views.help),
    path('aide/', views.aide),
    path('get_last_generated_csv/', views.get_last_generated_csv),
    path('get_product_links_hstatement/', views.get_product_links_hstatement),
    path('get_container_by_code/', views.get_container_by_code),
    path('verif_duplicate/', views.verif_duplicate),
   # path('containerlistview2/', ContainerListView2.as_view()),
   # path('container_search/', ContainerSearchListView.as_view()),
]

