from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ngettext
from django.template.response import TemplateResponse
from django.core import serializers
from django.shortcuts import render
from django.templatetags.static import static
from django.db.models import Q 
from django.utils.html import format_html
from django.utils.safestring import mark_safe    

import logging
from datetime import datetime, timedelta

import csv, random, string, shutil, os
from pathlib import Path
from django.conf import settings

from .models import HStatement, Provider, Place, Borrow_place, Product, Keyword, Demand, DemandCheck,  Command, Container_Base, Container, Container_Suppr

class DemandAdmin(admin.ModelAdmin):
    fields = ('product', 'other_infos', 'demandeur', 'place', 'capacity', 'quantity', 'uprice', 'provider', 'reference', 'physical_state', 'conditioning', 'complement', 'pureness', 'SDS_link_C', 'product_link', 'date_demanded', 'date_modified')
    list_display = ('provider', 'demandeur', 'date_modified', 'date_demanded', 'distributor', 'product_icon_link', 'SDS_link_C', 'reference', 'capacity', 'uprice', 'quantity', 'total_price_ET')
    readonly_fields = ('SDS_link_C', 'product_link', 'other_infos', 'date_demanded', 'date_modified')
    search_fields = ['provider__name', 'demandeur', 'product__cas_number']
    exclude= ('date_commanded','date_received',)
    autocomplete_fields = ['product']
    actions = ['command_action', ]  
    change_list_template= 'admin/change_list_demand.html'
    change_form_template= 'admin/change_form_demand.html'
    def command_action(self, request, queryset):
        if request.POST.get("command_action") is None:
            context = {"queryset": queryset}
            return render(request, "command_confirmation.html", context) 
        sdform_data = request.POST.dict()
        num_order =  sdform_data.get("num_order")
        for d in queryset:
            logging.debug(' d2c -' + str(d.id) + '-')
            new_command= Command(product=d.product, demandeur=d.demandeur, place=d.place, quantity=d.quantity, uprice=d.uprice, physical_state=d.physical_state, conditioning=d.conditioning, provider=d.provider, reference=d.reference, complement=d.complement, pureness=d.pureness, capacity=d.capacity, num_order=num_order, date_commanded= datetime.now(), date_modified= datetime.now())
            new_command.save()
        count_qs= queryset.count()
        queryset.delete()
        self.message_user(request, str(count_qs) + ' demands was successfully commanded.', level=messages.SUCCESS)
    command_action.short_description = "Command selected Demands"
    command_action.allowed_permissions = ('change',)
admin.site.register(Demand, DemandAdmin)

class DemandCheckAdmin(DemandAdmin):
    list_display = ('provider', 'demandeur', 'date_demanded', 'distributor', 'product_link', 'SDS_link_C', 'reference', 'capacity', 'uprice', 'quantity')
    pass
admin.site.register(DemandCheck, DemandCheckAdmin)

def recycle_codes():
    recycle_dbg= ''
    # List all containers codes (except the None one, only one I think) and only keep code
    list_codes_container2 = Container_Base.objects.filter(~Q(code=None))
    if list_codes_container2.count()==0 : return False
    #recycle_dbg+= 'list_codes_container2.count : '+str(list_codes_container2.count())
    list_codes_container= list_codes_container2.values_list('code', flat=True)
    recycle_dbg+= 'list_codes_container.count : '+str(list_codes_container.count())
    # create a list with all the integers between 1 and the highest code
    ints_list = range(1, max(list_codes_container))
    recycle_dbg+= '  max(list_codes_container) : '+str(max(list_codes_container))
    # create the list with all missing codes
    missing_codes_container = [x for x in ints_list if x not in list_codes_container]
    recycle_dbg+= '  len(missing_codes_container) : '+str(len(missing_codes_container))
    time_threshold = datetime.now() - timedelta(weeks=106) # ~ 2 years
    # list all Suppr more recent than time_threshold and only keep code
    list_suppr2 = Container_Suppr.objects.filter(date_modified__gt=time_threshold).filter(~Q(code=None)).order_by('code')
    list_suppr= list_suppr2.values_list('code', flat=True)
    recycle_dbg+= '  list_suppr.count : '+str(list_suppr.count())
    recycle= []; #quant= 100; ind_miss= 0; 
    for miss in missing_codes_container:
        if miss not in list_suppr: 
            #ind_miss+= 1
            recycle.append(miss)
        ##if miss in list_suppr: Shell.debug+= '_TooRecent_'+str(miss)+'_'
        #if ind_miss>quant: break
    #topN= recycle[:50]
    #recycle_dbg+= '  len(recycle) : '+str(len(recycle))
    #if len(recycle)== 0 : 
    #    inc_code= max(list_codes_container)+1
    #    for ci in range(100):
    #        recycle.append(inc_code+ci)
    #logging.debug('recycle_dbg : ' + recycle_dbg)
    #recycle_dbg : list_codes_container.count : 4732  max(list_codes_container) : 14283  len(missing_codes_container) : 9551 (=14283-4732) list_suppr.count : 2176  len(recycle) : 7375 (=9551-2176)
    return recycle 

class CommandAdmin(admin.ModelAdmin):
    fields = ('product', 'other_infos', 'num_order', 'demandeur', 'place', 'quantity', 'uprice', 'physical_state', 'conditioning', 'provider', 'reference', 'complement', 'pureness', 'capacity', 'date_demanded', 'date_commanded')
    list_display = ('demandeur', 'num_order', 'product_link', 'SDS_link_C', 'provider', 'reference', 'uprice', 'quantity', 'place_risk', 'date_commanded')
    readonly_fields = ('other_infos',)
    search_fields = ['demandeur', 'num_order', 'product__cas_number']
    exclude= ('date_modified','date_received',)
    autocomplete_fields = ['product']
    actions = ['receive_action', ]
    change_list_template= 'admin/change_list_command.html'
    change_form_template= 'admin/change_form_command.html'
    def receive_action(self, request, queryset):
        if queryset.count()!=1 :
            self.message_user(request, "You can only receive one command at a time.", level=messages.ERROR)
            return
        if request.POST.get("receive_action") is None:
            recycle_codes_json = recycle_codes()
            context = {"queryset": queryset, 'max_code': Container_Base.objects.latest().code, 'recycle_codes': recycle_codes_json}
            return render(request, "receive_confirmation.html", context) 
        sdform_data = request.POST.dict()
        c= queryset.first(); rest= c.quantity
        for x in range(c.quantity):
            #logging.debug(' on -'+str(x+1+Container_Base.objects.latest().code))
            num_checkbox =  sdform_data.get("checkbox"+str(x+1))
            if num_checkbox!=None:
                new_container= Container_Base(product=c.product, demandeur=c.demandeur, place=c.place, uprice=c.uprice, physical_state=c.physical_state, conditioning=c.conditioning, provider=c.provider, reference=c.reference, complement=c.complement, pureness=c.pureness, capacity=c.capacity, num_order=c.num_order, date_commanded= c.date_commanded, date_received= datetime.now(), date_modified= datetime.now(), code=int(num_checkbox))
                new_container.save(); rest-= 1
        if rest==0:
            queryset.delete()
            self.message_user(request, 'Command successfully received, '+str(c.quantity)+' container(s) created.', level=messages.SUCCESS)
        else:
            self.message_user(request, 'Command partially received, '+str(c.quantity-rest)+' / '+str(c.quantity)+' container(s) created.', level=messages.SUCCESS)
            c.quantity= rest; c.save()
    receive_action.short_description = "Receive selected Command"
    receive_action.allowed_permissions = ('change',)
admin.site.register(Command, CommandAdmin)

class Container_BaseAdmin(admin.ModelAdmin):
    fields = ('Cname', 'product', 'other_infos', 'demandeur', 'place', 'capacity', 'code', 'in_stock', 'borrow_place', 'borrower', 'num_order', 'uprice', 'physical_state', 'conditioning', 'provider', 'reference', 'complement', 'pureness', 'date_demanded', 'date_commanded', 'date_received')
    list_display = ('code', 'stock_loc', 'place_risk', 'product_link', 'complement', 'SDS_link_C', 'get_hazard_statements_pictos', 'other_infos', 'date_modified')
    search_fields = ['code', 'product__cas_number', 'product__name', 'place__code']
    autocomplete_fields = ['product']
    exclude= ('date_modified',)
    readonly_fields = ('SDS_link_C','Cname', 'other_infos')
    actions = ['borrow_action', 'return_action', 'suppress_container', 'copy_container','export_csv']
    change_list_template= 'admin/change_list_container.html'
    change_form_template= 'admin/change_form_container.html'
    # https://stackoverflow.com/questions/19203067/exclude-fields-in-django-admin-for-users-other-than-superuser    get_exclude replaced by get_readonly_fields
    #def get_readonly_fields(self, request, obj=None):
    #    readonly = super().get_readonly_fields(request, obj) or [] # get overall readonly fields
    #    if not request.user.is_superuser: # if user is not a superuser
    #        return readonly + ('code',)
    #    return readonly # otherwise return the default readonly fields if any
    # 
    def export_csv(self, request, queryset):
        for p in Path(settings.PROJECT_PATH).glob("last*.csv"):
            p.unlink()
        #https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits
        #filename= ''.join(random.choices(string.ascii_uppercase + string.digits, k=13))+'.csv'
        #filedir= static('/Exported_CSV_files/')
        #filepath= '/home/user/aspic2_project/aspic2_project'+filedir;
        #shutil.rmtree(filepath)
        #os.makedirs(filepath, exist_ok=True)
        filename= 'last'+''.join(random.choices(string.ascii_uppercase + string.digits, k=13))+'.csv'
        file_location= settings.PROJECT_PATH+filename
        f = open(file_location, 'w')
        writer = csv.writer(f)
        writer.writerow(["code", "product", "CAS", "borrow_place", "borrower", "place", "demandeur", "date_modified"])
        for s in queryset:
            writer.writerow([s.code, s.product.name, s.product.cas_number, s.borrow_place, s.borrower, s.place, s.demandeur, s.date_modified])
        count_qs= queryset.count()
        self.message_user(request, mark_safe(str(count_qs) + ' containers was successfully exported to the file <a href="../get_last_generated_csv">'+filename+'</a>'), level=messages.SUCCESS)
        #self.message_user(request, mark_safe(str(count_qs) + ' containers was successfully exported to the file <a href="'+filedir+filename+'">'+filename+'</a>'), level=messages.SUCCESS)
    export_csv.short_description = "Create a CSV from selected Containers"
    export_csv.allowed_permissions = ('view',)
    def changeform_view(self, request, obj_id, form_url, extra_context=None):
        extra_context = extra_context or {}
        res_recycle_codes = recycle_codes()
        extra_context['res_recycle_codes'] = res_recycle_codes
        return super(Container_BaseAdmin, self).changeform_view(request, obj_id, form_url, extra_context=extra_context)
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['search_placeholder'] = 'Search in code, CAS, name or place'
        return super(Container_BaseAdmin, self).changelist_view(request, extra_context=extra_context)
    def suppress_container(self, request, queryset):
        for d in queryset:
            #logging.debug(' c2c -' + str(d.id) + '-')
            new_container_suppr= Container_Suppr(product=d.product, demandeur=d.demandeur, place=d.place, uprice=d.uprice, physical_state=d.physical_state, conditioning=d.conditioning, provider=d.provider, reference=d.reference, complement=d.complement, pureness=d.pureness, capacity=d.capacity, num_order=d.num_order, code=d.code, in_stock=d.in_stock, borrow_place=d.borrow_place, borrower=d.borrower, date_demanded=d.date_demanded, date_commanded= datetime.now(), date_received=d.date_received, date_modified= datetime.now())
            new_container_suppr.save()
        count_qs= queryset.count()
        queryset.delete()
        self.message_user(request, str(count_qs) + ' containers was successfully suppressed.', level=messages.SUCCESS)
    suppress_container.short_description = "Suppress selected Containers"
    suppress_container.allowed_permissions = ('view',)
    #suppress_container.allowed_permissions = ('change',)
    def return_action(self, request, queryset):
        if request.POST.get("return_action") is None:
            return render(request, "return_confirmation.html", {'queryset': queryset,})
        count_qs= queryset.count()
        verif_stock= queryset.filter(in_stock=True)
        if verif_stock:
            self.message_user(request, "Something went wrong, maybe a container is already returned.", level=messages.ERROR)
        else :
            updated= queryset.update(borrower= '', date_modified= datetime.now(), borrow_place= None, in_stock= True)
            self.message_user(request, str(count_qs) + ' container was successfully returned.', level=messages.SUCCESS)
    return_action.short_description = "Return selected Containers"
    return_action.allowed_permissions = ('view',)
    def borrow_action(self, request, queryset):
        if request.POST.get("borrow_action") is None:
            Borrow_place_json = serializers.serialize('json', Borrow_place.objects.all())
            context = {"queryset": queryset, 'Borrow_place_json':Borrow_place_json}
            return render(request, "borrow_confirmation.html", context) 
        count_qs= queryset.count()
        verif_stock= queryset.filter(in_stock=False)
        if verif_stock:
            self.message_user(request, "Something went wrong, maybe a container is already borrowed.", level=messages.ERROR)
        else :
            sdform_data = request.POST.dict()
            hallway =  sdform_data.get("selecthallway"); room =  sdform_data.get("selectroom"); borrower =  sdform_data.get("borrower");
            borrow_place= None ; 
            try:
                borrow_place= Borrow_place.objects.get(hallway=hallway, room=room)
            except ObjectDoesNotExist:
                pass
            logging.debug(' borrower -' + borrower + '-')
            updated= queryset.update(borrower= borrower, date_modified= datetime.now(), borrow_place= borrow_place, in_stock= False)
            self.message_user(request, str(count_qs) + ' container was successfully borrowed.', level=messages.SUCCESS)
    borrow_action.short_description = "Borrow selected Containers"
    borrow_action.allowed_permissions = ('view',)
    def copy_container(self, request, queryset):
        max_copy= 10
        if queryset.count()!=1 :
            self.message_user(request, "You can only copy one container at a time.", level=messages.ERROR)
            return
        if request.POST.get("copy_container") is None:
            recycle_codes_json = recycle_codes()
            context = {"queryset": queryset, 'max_code': Container_Base.objects.latest().code, 'recycle_codes': recycle_codes_json, 'max_copy':max_copy}
            return render(request, "copy_container.html", context) 
        sdform_data = request.POST.dict()
        c= queryset.first(); num_copy= 0 
        for x in range(max_copy):
            #logging.debug(' on -'+str(x+1+Container_Base.objects.latest().code))
            num_checkbox =  sdform_data.get("checkbox"+str(x+1))
            if num_checkbox!=None:
                new_container= Container_Base(product=c.product, demandeur=c.demandeur, place=c.place, uprice=c.uprice, physical_state=c.physical_state, conditioning=c.conditioning, provider=c.provider, reference=c.reference, complement=c.complement, pureness=c.pureness, capacity=c.capacity, in_stock=c.in_stock, borrow_place= c.borrow_place, borrower= c.borrower, num_order=c.num_order, date_commanded= c.date_commanded, date_received= c.date_received, date_modified= datetime.now(), code=int(num_checkbox))
                new_container.save(); num_copy+= 1
        self.message_user(request, str(num_copy)+' container(s) successfully copied, ', level=messages.SUCCESS)
    copy_container.short_description = "Copy selected Container"
    copy_container.allowed_permissions = ('change',)
#admin.site.register(Container_Base, Container_BaseAdmin)

#class Container_View1Admin(ContainerAdmin):
#    list_filter = ('place__code', 'borrow_place__hallway','borrow_place__room',)
#    change_list_template= 'admin/change_list_container_view1.html'
    #pass
#admin.site.register(Container_View1, Container_View1Admin)

# class used by the class below Container_View2Admin(ContainerAdmin):
# https://hakibenita.com/how-to-add-a-text-filter-to-django-admin
class InputFilter(admin.SimpleListFilter):
    template = 'admin/input_text_filter.html'
    def lookups(self, request, model_admin):
        return ((),)# Dummy, required to show the filter.
    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super().choices(changelist))
        all_choice['query_parts'] = (
            (k, v)
            for k, v in changelist.get_filters_params().items()
            if k != self.parameter_name
        )
        yield all_choice
class DemandeurFilter(InputFilter):
    parameter_name = 'demandeur'
    title = 'demandeur'
    def queryset(self, request, queryset):
        if self.value() is not None:
            demandeur = self.value()
            #return queryset.filter( Q(code=uid) | Q(place__code=uid) )
            return queryset.filter(demandeur__icontains=demandeur)
class ComplementFilter(InputFilter):
    parameter_name = 'complement'
    title = 'complement'
    def queryset(self, request, queryset):
        if self.value() is not None:
            complement = self.value()
            return queryset.filter(complement__icontains=complement)
class FormulaFilter(InputFilter):
    parameter_name = 'formula'
    title = 'formula'
    def queryset(self, request, queryset):
        if self.value() is not None:
            formula = self.value()
            return queryset.filter(product__formula__icontains=formula)
#https://stackoverflow.com/questions/5429276/how-to-change-the-django-admin-filter-to-use-a-dropdown-instead-of-list/39618385
class DropdownFilter(admin.AllValuesFieldListFilter):
    template = 'admin/dropdown_filter.html'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title= self.field_path # place__code # self.field aspic2.Place.Code

class ContainerAdmin(Container_BaseAdmin):
    list_display = ('code', 'stock_loc', 'place_risk', 'product_link', 'pureness', 'complement', 'SDS_link_C', 'get_hazard_statements_pictos', 'other_infos', 'date_modified', 'date_received')
    list_filter = (('place__code', DropdownFilter), ('borrow_place__hallway', DropdownFilter), ('borrow_place__room', DropdownFilter), 'date_modified', 'date_received',ComplementFilter ,FormulaFilter , DemandeurFilter, )
    change_list_template= 'admin/change_list_container_view2.html'
    search_fields = ['code', 'product__cas_number', 'product__name', 'place__code', 'borrower']
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['search_placeholder'] = 'Search in No, CAS, name, borrower or place'
        return super(Container_BaseAdmin, self).changelist_view(request, extra_context=extra_context)
admin.site.register(Container, ContainerAdmin)

class Container_SupprAdmin(admin.ModelAdmin):
    fields = ('Cname', 'product', 'other_infos', 'place', 'code', 'in_stock', 'borrow_place', 'borrower', 'num_order', 'demandeur', 'uprice', 'physical_state', 'conditioning', 'provider', 'reference', 'complement', 'pureness', 'capacity', 'date_demanded', 'date_commanded', 'date_received')
    list_display = ('code', 'stock_loc', 'place_risk', 'product_link', 'SDS_link_C', 'get_hazard_statements_pictos', 'date_modified', 'date_received')
    search_fields = ['code', 'product__cas_number', 'product__name']
    autocomplete_fields = ['product']
    exclude= ('date_modified',)
    readonly_fields = ('SDS_link_C', 'other_infos','Cname')
    actions = ['restore_container', ]
    change_list_template= 'admin/change_list_container_suppr.html'
    change_form_template= 'admin/change_form_container_suppr.html'
    def restore_container(self, request, queryset):
        for d in queryset:
            if Container_Base.objects.filter(code = d.code ).exists():
                self.message_user(request, "Cannot restore containers because at least one containers has a duplicate code.", level=messages.ERROR)
                return
        for d in queryset:
            #logging.debug(' c2c -' + str(d.id) + '-')
            new_container= Container_Base(product=d.product, demandeur=d.demandeur, place=d.place, uprice=d.uprice, physical_state=d.physical_state, conditioning=d.conditioning, provider=d.provider, reference=d.reference, complement=d.complement, pureness=d.pureness, capacity=d.capacity, num_order=d.num_order, code=d.code, in_stock=d.in_stock, borrow_place=d.borrow_place, borrower=d.borrower, date_demanded=d.date_demanded, date_commanded= datetime.now(), date_received=d.date_received, date_modified= datetime.now())
            new_container.save()
        count_qs= queryset.count()
        queryset.delete()
        self.message_user(request, str(count_qs) + ' containers was successfully restored.', level=messages.SUCCESS)
    restore_container.short_description = "Restore selected Containers"
    restore_container.allowed_permissions = ('view',) # ('change',)
admin.site.register(Container_Suppr, Container_SupprAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display= ('name', 'SDS_link', 'verif', 'get_hazard_statements', 'get_hazard_statements_pictos', 'modified_date', 'no_info', 'no_danger', 'Attention', 'H_ACID', 'H_BASE', 'H_AB', 'H_F', 'formula', 'id')
    fields= ('name', 'cas_number', 'CAS_infos', 'SDS_link', 'verif', 'get_hazard_statements_pictos', 'formula', 'hazard_statements', ('no_info', 'no_danger', 'Attention', 'H_ACID', 'H_BASE', 'H_AB', 'H_F'), 'Hazard_infos', 'keywords', 'modified_date')
    readonly_fields = ('CAS_infos', 'SDS_link', 'get_hazard_statements_pictos', 'Hazard_infos', 'modified_date', )
    #search_fields = ['cas_number', 'name', 'formula']
    search_fields = ['cas_number', 'name', 'formula', 'keywords__name']
    filter_horizontal = ('hazard_statements', 'keywords',)
    change_list_template= 'admin/change_list_product.html'
    change_form_template= 'admin/change_form_product.html'
    # https://stackoverflow.com/questions/19203067/exclude-fields-in-django-admin-for-users-other-than-superuser    get_exclude replaced by get_readonly_fields
    # https://stackoverflow.com/questions/4789021/in-django-how-do-i-check-if-a-user-is-in-a-certain-group
    def get_readonly_fields(self, request, obj=None):
        readonly = super().get_readonly_fields(request, obj) or [] # get overall readonly fields
        if request.user.groups.filter(name = 'borrower').exists():
            return readonly + ('verif',)
        return readonly # otherwise return the default readonly fields if any
admin.site.register(Product, ProductAdmin)

class PlaceAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'risk')
    fields = ('code', 'name', 'risk', 'barcode')
    readonly_fields = ('barcode',)
    search_fields = ['id', 'code', 'name']
    change_form_template= 'admin/change_form_place.html'
admin.site.register(Place, PlaceAdmin)

class Borrow_placeAdmin(admin.ModelAdmin):
    list_display = ('id', 'hallway', 'room')
    fields = ('id', 'hallway', 'room')
    readonly_fields = ('id',)
    search_fields = ['id', 'hallway', 'room']
admin.site.register(Borrow_place, Borrow_placeAdmin)

class ProviderAdmin(admin.ModelAdmin):
    list_display = ('name', 'distributor')
    search_fields = ['name', 'distributor']
admin.site.register(Provider, ProviderAdmin)

class HStatementAdmin(admin.ModelAdmin):
    list_display = ('code', 'Hazard_statement', 'pictos_list')
    fields= ('code', 'Hazard_statement', ('GHS01', 'GHS02', 'GHS03', 'GHS04', 'GHS05', 'GHS06', 'GHS07', 'GHS08', 'GHS09'))
    search_fields = ['code', 'Hazard_statement']
    change_list_template= 'admin/change_list_hstatement.html'
    change_form_template= 'admin/change_form_hstatement.html'
admin.site.register(HStatement, HStatementAdmin)

class KeywordAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
admin.site.register(Keyword, KeywordAdmin)

